import 'package:flutter/material.dart';

class home_screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return make_it_rain();
  }
}

class make_it_rain extends State<home_screen> {
  int money = 1;

  void increment() {
    setState(() {
      money++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Text("Make it Rain",
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
      body: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.all(10),
              child: Center(
                  child: Text("Get Rich",
                      style: TextStyle(
                          color: Colors.blue[900],
                          fontWeight: FontWeight.bold,
                          fontSize: 30)))),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("$money",
                style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                    fontSize: 35)),
          ),
          Container(
              width: 125,
              height: 40,
              child: RaisedButton(
                  onPressed: increment,
                  color: Colors.blue[900],
                  child: Text("Rain it",
                      style: TextStyle(color: Colors.white, fontSize: 20))))
        ],
      ),
    );
  }
}
